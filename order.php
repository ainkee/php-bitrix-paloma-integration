<?php
/**
 * Файл order.php использует класс OrderManager для обработки заказов.
 * Для корректной работы класса необходимо передать ему массив данных запроса со следующими полями:
 *
 * - 'deal_id': ID сделки, число (обязательное, по умолчанию null).
 * - 'point': Название точки обработки заказа, строка (обязательное, по умолчанию пустая строка).
 * - 'name': Имя клиента, строка (обязательное, по умолчанию пустая строка).
 * - 'phone': Телефон клиента, строка (обязательное, по умолчанию пустая строка).
 * - 'email': Email клиента, строка (необязательное, по умолчанию пустая строка).
 * - 'address': Адрес доставки, строка (обязательное, по умолчанию пустая строка).
 * - 'comment': Комментарий к заказу, строка (необязательное, по умолчанию пустая строка).
 * - 'person_amount': Количество человек, число (необязательное, по умолчанию 1).
 * - 'total_price': Итоговая сумма заказа, число (обязательное, по умолчанию 0).
 * - 'discount_amount': Сумма скидки, число (необязательное, по умолчанию 0).
 * - 'delivery_type': Тип доставки, число (необязательное, по умолчанию 1).
 * - 'is_cash': Признак оплаты наличными, булево значение (необязательное, по умолчанию false).
 * - 'is_payed': Признак выполнения оплаты, булево значение (необязательное, по умолчанию false).
 */

require_once __DIR__ . '/init.php';

use BitrixLib\Client\Bitrix24Client;
use CNTL\Manager\OrderManager;
use GuzzleHttp\Exception\GuzzleException;

const DELIVERY_TYPE_DELIVERY = 1;
const DELIVERY_TYPE_PICKUP = 2;


$bitrix_api = new Bitrix24Client();
$request = validate($_REQUEST);

$manager = new OrderManager($bitrix_api);

try {
    $manager->processOrder($request);
} catch (GuzzleException|Exception $e) {
}


/**
 * Валидация данных запроса.
 *
 * @param array $request
 * @return array
 */
function validate(array $request): array
{
    $request['phone'] = getFirstValue($request['phone']);
    $request['email'] = getFirstValue($request['email']);
    $request['delivery_type'] = getDeliveryTypeId($request['delivery_type']);
    if (empty($request['address'])) {
        $request['delivery_type'] = DELIVERY_TYPE_PICKUP;
    }
    $request['comment'] = "Вид оплаты: {$request['payment_type']}\n"
        . str_replace("<br>", "\n", $request['comment']);
    $request['is_cash'] = ($request['payment_type'] === 'Наличный');
    $request['is_payed'] = !empty($request['is_payed']);

    return $request;
}

/**
 * Получение первого значения из строки значений, разделенных запятой.
 *
 * @param string $string
 * @return string
 */
function getFirstValue(string $string): string
{
    return explode(",", $string)[0];
}

/**
 * Получение ID указанного типа доставки.
 *
 * @param string $deliveryType
 * @return int
 */
function getDeliveryTypeId(string $deliveryType): int
{
    return $deliveryType === 'Доставка' ? DELIVERY_TYPE_DELIVERY : DELIVERY_TYPE_PICKUP;
}
<?php

use BitrixLib\Exceptions\ApiException;
use CNTL\Manager\OrderStatusManager;
use GuzzleHttp\Exception\GuzzleException;

require_once __DIR__ . '/init.php';

$stages = explode(',', $_ENV['BITRIX_PROCESSING_ORDER_STAGES']);
$processed_stage = $_ENV['BITRIX_PROCESSED_ORDER_STAGE'];

$manager = new OrderStatusManager($stages, $processed_stage);
try {
    $manager->processOrders();
} catch (ApiException|GuzzleException $e) {
    echo $e->getMessage();
}
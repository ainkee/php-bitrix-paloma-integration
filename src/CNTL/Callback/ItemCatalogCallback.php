<?php

namespace CNTL\Callback;

use BitrixLib\Api\General\Batch;
use BitrixLib\Exceptions\ApiException;
use CNTL\ProductCatalog\CatalogItem;
use GuzzleHttp\Exception\GuzzleException;

class ItemCatalogCallback implements CatalogCallbackInterface
{
    private array $items;
    private Batch $batchProductsList;
    private Batch $batchProductsAdd;

    public function __construct()
    {
        $this->items = [];
        $this->batchProductsList = new Batch();
        $this->batchProductsAdd = new Batch();
    }

    /**
     * @param CatalogItem $catalogItem
     * @throws ApiException
     * @throws GuzzleException
     */
    public function execute(CatalogItem $catalogItem): void
    {
        $this->items[] = $catalogItem;
        $this->getItemData($catalogItem);
    }

    /**
     * @throws GuzzleException
     * @throws ApiException
     */
    public function finish(): void
    {
        $products = $this->batchProductsList->getFormattedResult();
        foreach ($this->items as $key => $catalogItem) {
            $this->updateOrCreateItem($products[$key] ?? [], $catalogItem);
        }
        $this->batchProductsAdd->getResult();
    }

    /**
     * Составляет запрос для получения данных товара из Битрикс.
     *
     * @param CatalogItem $catalogItem Объект продукта для получения данных.
     * @return void Массив данных о товаре.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    private function getItemData(CatalogItem $catalogItem): void
    {
        $this->batchProductsList->addToBatch("crm.product.list", [
            "filter" => [
                "CODE" => $catalogItem->uid,
            ],
            "select" => ["*"],
        ]);
    }

    /**
     * Обновляет или создает товар в зависимости от наличия данных.
     *
     * @param array $productData Массив данных о существующем товаре.
     * @param CatalogItem $catalogItem Объект товара для обновления или создания.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    private function updateOrCreateItem(array $productData, CatalogItem $catalogItem): void
    {
        if (!empty($productData)) {
            $this->updateItem($productData, $catalogItem);
        } else {
            $this->createItem($catalogItem);
        }
    }

    /**
     * Обновляет данные товара.
     *
     * @param array $productData Массив данных о товаре для обновления.
     * @param CatalogItem $catalogItem Объект товара для обновления.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    private function updateItem(array $productData, CatalogItem $catalogItem): void
    {
        $catalogItem->setExternalId($productData["ID"]);
        $this->batchProductsAdd->addToBatch("crm.product.update", [
            "id" => $catalogItem->getExternalId(),
            "fields" => $this->getItemFields($catalogItem)
        ]);
    }

    /**
     * Создает новый товар.
     *
     * @param CatalogItem $catalogItem Объект товара для создания.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    private function createItem(CatalogItem $catalogItem): void
    {
        $this->batchProductsAdd->addToBatch("crm.product.add", ["fields" => $this->getItemFields($catalogItem)]);
    }

    /**
     * Возвращает поля товара для создания или обновления.
     *
     * @param CatalogItem $catalogItem Объект товара.
     * @return array Массив полей товара.
     */
    private function getItemFields(CatalogItem $catalogItem): array
    {
        return [
            "NAME" => $catalogItem->name,
            "SECTION_ID" => $this->getParentSectionId($catalogItem),
            "PRICE" => $catalogItem->price,
            "ACTIVE" => $catalogItem->active,
            "CODE" => $catalogItem->uid,
            $_ENV['BITRIX_PALOMA_OBJECT_ID_PROPERTY'] => $catalogItem->objectId
        ];
    }

    /**
     * Возвращает внешний идентификатор родительского раздела каталога или товара.
     *
     * @param CatalogItem $catalog Объект каталога.
     * @return string Идентификатор родительского раздела.
     */
    private function getParentSectionId(CatalogItem $catalog): string
    {
        return $catalog->getParent() ? $catalog->getParent()->getExternalId() : '';
    }
}

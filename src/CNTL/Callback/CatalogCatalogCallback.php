<?php

namespace CNTL\Callback;

use BitrixLib\Api\CRM\ProductSection;
use BitrixLib\Api\Filter\NameFilter;
use BitrixLib\Api\Filter\RootSectionFilter;
use BitrixLib\Api\Filter\SectionIdFilter;
use BitrixLib\Exceptions\ApiException;
use CNTL\ProductCatalog\CatalogItem;
use GuzzleHttp\Exception\GuzzleException;

class CatalogCatalogCallback implements CatalogCallbackInterface
{
    /**
     * @throws ApiException
     * @throws GuzzleException
     */
    public function execute(CatalogItem $catalogItem): void
    {
        $sectionData = $this->getSectionData($catalogItem);
        $this->updateOrCreateSection($sectionData, $catalogItem);
    }

    public function finish(): void
    {
    }

    /**
     * Получает данные раздела из Битрикс.
     *
     * @param CatalogItem $catalogItem Объект каталога, для которого нужно получить данные раздела.
     * @return array Массив данных о разделе.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    private function getSectionData(CatalogItem $catalogItem): array
    {
        $filters = [new NameFilter($catalogItem->name)];
        $section = $this->getParentSectionId($catalogItem);
        $filters[] = $section != '' ? new SectionIdFilter($section) : new RootSectionFilter();
        return ProductSection::getFilteredList($filters);
    }

    /**
     * Обновляет или создает раздел в зависимости от наличия данных.
     *
     * @param array $sections Массив данных о существующих разделах.
     * @param CatalogItem $catalogItem Объект каталога для обновления или создания раздела.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    private function updateOrCreateSection(array $sections, CatalogItem $catalogItem): void
    {
        if (isset($sections[0]["ID"])) {
            $catalogItem->setExternalId($sections[0]["ID"]);
        } else {
            $this->createSection($catalogItem);
        }
    }

    /**
     * Создает новый раздел для каталога.
     *
     * @param CatalogItem $catalogItem Объект каталога для создания нового раздела.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    private function createSection(CatalogItem $catalogItem): void
    {
        $catalogItem->setExternalId(ProductSection::add([
            "NAME" => $catalogItem->name,
            "SECTION_ID" => $this->getParentSectionId($catalogItem),
        ])["result"]);
    }

    /**
     * Возвращает внешний идентификатор родительского раздела каталога или товара.
     *
     * @param CatalogItem $catalogItem Объект каталога.
     * @return string Идентификатор родительского раздела.
     */
    private function getParentSectionId(CatalogItem $catalogItem): string
    {
        return $catalogItem->getParent() ? $catalogItem->getParent()->getExternalId() : '';
    }
}
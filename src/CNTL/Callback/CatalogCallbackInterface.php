<?php

namespace CNTL\Callback;

use CNTL\ProductCatalog\CatalogItem;

interface CatalogCallbackInterface
{
    public function execute(CatalogItem $catalogItem): void;
    public function finish(): void;
}
<?php

namespace CNTL\Manager;

use BitrixLib\Api\CRM\Deal;
use BitrixLib\Api\CRM\Product;
use BitrixLib\Client\Bitrix24Client;
use BitrixLib\Exceptions\ApiException;
use CNTL\Api\Paloma\Order;
use CNTL\Api\Paloma\Point;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс OrderManager отвечает за обработку и отправление заказа в Paloma365.
 */
class OrderManager
{
    private Bitrix24Client $bitrixApi;

    /**
     * Конструктор класса OrderManager.
     * @param Bitrix24Client $bitrixApi Экземпляр клиента API Bitrix24.
     */
    public function __construct(Bitrix24Client $bitrixApi)
    {
        $this->bitrixApi = $bitrixApi;
    }

    /**
     * Обработка и добавление в Paloma365 заказа.
     * @param array $requestData Данные запроса.
     * @throws Exception
     * @throws GuzzleException
     */
    public function processOrder(array $requestData): void
    {
        $dealId = $requestData['deal_id'] ?? null;
        $point = $requestData['point'] ?? '';

        $products = $this->getDealProducts($dealId);
        $orderItems = $this->formatOrderItems($products);

        $order = $this->formatOrderData($requestData, $orderItems, $dealId);

        if ($point && $order) {
            $this->addOrder($point, $order);
        }
    }

    /**
     * Получение продуктов из сделки.
     * @param mixed $dealId ID сделки.
     * @return array Массив продуктов.
     * @throws ApiException
     * @throws GuzzleException
     */
    private function getDealProducts(mixed $dealId): array
    {
        return $this->bitrixApi->call('crm.deal.productrows.get', ['id' => $dealId])['result'] ?? [];
    }

    /**
     * Форматирование элементов заказа.
     * @param array $products Массив продуктов.
     * @return array Форматированные элементы заказа.
     * @throws ApiException
     * @throws GuzzleException
     */
    private function formatOrderItems(array $products): array
    {
        $orderItems = [];
        foreach ($products as $productRow) {
            $product = Product::get($productRow['PRODUCT_ID'])["result"];
            $orderItems[] = [
                'object_id' => $product[$_ENV['BITRIX_PALOMA_OBJECT_ID_PROPERTY']]["value"],
                'name' => $productRow['PRODUCT_NAME'],
                'count' => $productRow['QUANTITY'],
                'price' => $productRow['PRICE'],
            ];
        }
        return $orderItems;
    }

    /**
     * Форматирование данных заказа.
     * @param array $requestData Данные запроса.
     * @param array $orderItems Элементы заказа.
     * @param mixed $dealId ID сделки.
     * @return array Форматированные данные заказа.
     */

    private function formatOrderData(array $requestData, array $orderItems, mixed $dealId): array
    {
        return [
            'order_id' => $dealId,
            'date' => date('Y-m-d H:i:s', strtotime('+2 hours', time())),
            'name' => $requestData['name'] ?? '',
            'phone' => $requestData['phone'] ?? '',
            'email' => $requestData['email'] ?? '',
            'address' => $requestData['address'] ?? '',
            'comment' => $requestData['comment'],
            'person_amount' => $requestData['person_amount'] ?? 1,
            'total_price' => $requestData['total_price'] ?? 0,
            'discount_amount' => $requestData['discount_amount'] ?? 0,
            'exchange' => $requestData['total_price'],
            'delivery_type' => $requestData['delivery_type'] ?? 1,
            'is_cash' => $requestData['is_cash'] ?? false,
            'is_payed' => $requestData['is_payed'] ?? false,
            'order_items' => $orderItems,
        ];
    }

    /**
     * Добавление заказа.
     * @param mixed $point Название точки продажи.
     * @param array $order Данные заказа.
     * @throws Exception
     * @throws GuzzleException
     */
    private function addOrder(mixed $point, array $order): void
    {
        $result = Order::add($point, $order);
        $palomaOrderId = $result['paloma_order_id'] ?? null;

        if (!$palomaOrderId) {
            throw new Exception('Ошибка при добавлении заказа в Paloma365');
        } else {
            Deal::update($order['order_id'], [
                $_ENV['BITRIX_PALOMA_ORDER_ID_FIELD'] => $palomaOrderId,
                "STAGE_ID" => $_ENV['BITRIX_ORDER_CREATED_STAGE']
            ]);
        }
    }
}

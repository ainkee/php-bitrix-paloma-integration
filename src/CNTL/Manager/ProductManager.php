<?php

namespace CNTL\Manager;

use CNTL\Api\Paloma\Product;
use CNTL\Callback\CatalogCatalogCallback;
use CNTL\Callback\ItemCatalogCallback;
use CNTL\ProductCatalog\CatalogIterator;
use CNTL\ProductCatalog\ProductTree;
use Exception;

/**
 * Класс ProductManager отвечает за синхронизацию продуктов с внешним сервисом.
 */
class ProductManager
{

    /**
     * Метод для запуска процесса синхронизации продуктов.
     * @throws Exception В случае возникновения исключений в процессе синхронизации.
     */
    public function synchronize(): void
    {
        $products = Product::getList();
        $tree = (new ProductTree($products))->get();

        $catalogCallback = new CatalogCatalogCallback();
        $itemCallback = new ItemCatalogCallback();

        $catalogIterator = new CatalogIterator($tree, $catalogCallback, $itemCallback);
        $catalogIterator->iterate();
    }
}
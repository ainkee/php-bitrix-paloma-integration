<?php

namespace CNTL\Manager;

use BitrixLib\Api\CRM\Deal;
use BitrixLib\Api\General\Batch;
use BitrixLib\Exceptions\ApiException;
use CNTL\Api\Paloma\Order;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс OrderStatusManager отвечает за управление статусами заказов.
 */
class OrderStatusManager
{
    /**
     * @var array Стадии обработки заказов.
     */
    private array $stages;

    /**
     * @var string Стадия завершенных заказов.
     */
    private string $processedStage;

    /**
     * @var Batch Объект для пакетной обработки операций.
     */
    private Batch $batch;

    /**
     * Конструктор класса.
     *
     * @param array $stages Стадии обработки заказов.
     * @param string $processedStage Стадия, на которую переводится заказ после завершения обработки.
     */
    public function __construct(array $stages, string $processedStage)
    {
        $this->stages = $stages;
        $this->processedStage = $processedStage;
        $this->batch = new Batch();
    }

    /**
     * Обрабатывает заказы на различных стадиях.
     *
     * @throws ApiException Если произошла ошибка API.
     * @throws GuzzleException Если произошла ошибка сетевого запроса.
     */
    public function processOrders(): void
    {
        foreach ($this->stages as $stage) {
            $this->processStage($stage);
        }

        $this->batch->getResult();
    }

    /**
     * Обрабатывает заказы на конкретной стадии.
     *
     * @param string $stage Текущая стадия обработки заказов.
     *
     * @throws ApiException Если произошла ошибка API.
     * @throws GuzzleException Если произошла ошибка сетевого запроса.
     * @throws Exception Если произошла общая ошибка.
     */
    private function processStage(string $stage): void
    {
        $deals = Deal::getAll(['STAGE_ID' => $stage]);

        foreach ($deals as $deal) {
            $this->processDeal($deal);
        }
    }

    /**
     * Обрабатывает отдельный заказ.
     *
     * @param array $deal Данные о сделке.
     *
     * @throws Exception Если произошла ошибка при обработке заказа.
     * @throws GuzzleException Если произошла ошибка сетевого запроса.
     */
    private function processDeal(array $deal): void
    {
        $status = Order::getStatus($deal['ID']);

        if ($this->isOrderCompleted($status)) {
            $this->updateDealStage($deal['ID']);
        }
    }

    /**
     * Проверяет, завершен ли заказ.
     *
     * @param array $status Статус заказа.
     *
     * @return bool Возвращает true, если заказ завершен.
     */
    private function isOrderCompleted(array $status): bool
    {
        return !empty($status['receipt_id']);
    }

    /**
     * Обновляет стадию заказа в CRM.
     *
     * @param int|string $id Идентификатор сделки.
     *
     * @throws ApiException Если произошла ошибка API.
     * @throws GuzzleException Если произошла ошибка сетевого запроса.
     */
    private function updateDealStage(int|string $id): void
    {
        $this->batch->addToBatch('crm.deal.update', [
            'id' => $id,
            'fields' => ['STAGE_ID' => $this->processedStage]
        ]);
    }
}
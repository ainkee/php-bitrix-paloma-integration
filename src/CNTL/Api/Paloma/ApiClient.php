<?php

namespace CNTL\Api\Paloma;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс ApiClient
 * Клиент для работы с API Paloma.
 */
class ApiClient
{
    /**
     * Базовый URL API Paloma.
     * @var string
     */
    private static string $api_url = 'https://api.paloma365.com/company/api/';

    /**
     * HTTP клиент для выполнения запросов.
     * @var Client
     */
    private Client $client;

    /**
     * Конструктор класса ApiClient.
     * Инициализирует HTTP клиент с базовым URI.
     */
    public function __construct()
    {
        $this->client = new Client(['base_uri' => static::$api_url]);
    }

    /**
     * Выполняет вызов API.
     *
     * @param array|string $query Параметры запроса.
     * @param array|string $json Тело запроса в формате JSON.
     * @param bool $post Использовать ли POST запрос.
     * @return array Ответ от API в виде массива.
     * @throws Exception Исключение при неудачном запросе к API.
     */
    public function call(array|string $query, array|string $json = [], bool $post = false): array
    {
        $query = array_merge_recursive($query, [
            "authkey" => $_ENV['PALOMA_API_KEY']
        ]);
        $options['query'] = $query;

        if ($post) {
            $options['json'] = $json;
        }

        try {
            $response = $this->client->request($post ? 'POST' : 'GET', static::$api_url, $options);
        } catch (GuzzleException $e) {
            throw new Exception('API Request failed: ' . $e->getMessage());
        }

        $statusCode = $response->getStatusCode();
        $body = json_decode($response->getBody()->getContents(), true);

        if ($statusCode >= 400) {
            throw new Exception($body['message'] ?? 'An error occurred');
        }

        return $body;
    }

    /**
     * Выполняет вызов API с использованием Tester.
     *
     * @param string $method Метод API для вызова.
     * @param array|string $query Параметры запроса.
     * @param array|string $json Тело запроса в формате JSON.
     * @param bool $post Использовать ли POST запрос.
     * @return array Ответ от API в виде массива.
     * @throws Exception Исключение при неудачном запросе к API.
     */
    public function callTester(string $method, array|string $query = [], array|string $json = [], bool $post = false): array
    {
        $query = array_merge_recursive($query, [
            'method' => $method,
            'class' => 'Tester'
        ]);
        return $this->call($query, $json, $post);
    }

    /**
     * Выполняет вызов API с использованием guide2xml.
     *
     * @param string $method Метод API для вызова.
     * @param array|string $query Параметры запроса.
     * @param array|string $json Тело запроса в формате JSON.
     * @param bool $post Использовать ли POST запрос.
     * @return array Ответ от API в виде массива.
     * @throws Exception Исключение при неудачном запросе к API.
     */
    public function callGuide2XML(string $method, array|string $query = [], array|string $json = [], bool $post = false): array
    {
        $query = array_merge_recursive($query, [
            'method' => 'to_file',
            'class' => 'guide2xml',
            'output_format' => 'json',
            'tables[0]' => $method
        ]);
        return $this->call($query, $json, $post);
    }
}
<?php

namespace CNTL\Api\Paloma;

use Exception;

/**
 * Абстрактный базовый класс для работы с сущностями через API Paloma365.
 */
abstract class BaseEntity
{
    /**
     * Статический клиент для обращения к API Paloma365.
     * @var ApiClient
     */
    protected static ApiClient $client;

    /**
     * Инициализирует клиент Paloma365 при первом обращении к классу.
     */
    public static function init(): void
    {
        if (!isset(static::$client)) {
            static::$client = new ApiClient();
        }
    }

    /**
     * Выполняет запрос к API Paloma365.
     *
     * @param array|string $query
     * @param array|string $json
     * @param bool $post
     * @return array Ответ от API в виде массива.
     * @throws Exception В случае ошибок сети.
     */
    protected static function call(array|string $query, array|string $json = [], bool $post = false): array
    {
        static::init();
        return static::$client->call($query, $json, $post);
    }

    /**
     * Выполняет вызов API с использованием Tester.
     *
     * Этот метод инициализирует клиента API (если это еще не было сделано) и делегирует выполнение запроса клиенту.
     *
     * @param string $method Метод API для вызова.
     * @param array|string $query Параметры запроса.
     * @param array|string $json Тело запроса в формате JSON.
     * @param bool $post Использовать ли POST запрос.
     * @return array Ответ от API в виде массива.
     * @throws Exception Исключение при неудачном запросе к API.
     */
    protected static function callTester(string $method, array|string $query = [], array|string $json = [], bool $post = false): array
    {
        static::init();
        return static::$client->callTester($method, $query, $json, $post);
    }

    /**
     * Выполняет вызов API с использованием guide2xml.
     *
     * Этот метод инициализирует клиента API и делегирует выполнение запроса клиенту, используя guide2xml для форматирования данных.
     *
     * @param string $method Метод API для вызова.
     * @param array|string $query Параметры запроса.
     * @param array|string $json Тело запроса в формате JSON.
     * @param bool $post Использовать ли POST запрос.
     * @return array Ответ от API в виде массива.
     * @throws Exception Исключение при неудачном запросе к API.
     */
    protected static function callGuide2XML(string $method, array|string $query = [], array|string $json = [], bool $post = false): array
    {
        static::init();
        return static::$client->callGuide2XML($method, $query, $json, $post);
    }
}
<?php

namespace CNTL\Api\Paloma;

use Exception;

/**
 * Класс Order
 * Расширяет функциональность BaseEntity для работы с заказами в системе Paloma.
 */
class Order extends BaseEntity
{
    /**
     * Получает статус заказа по его идентификатору.
     *
     * Вызывает метод API 'status' с переданным идентификатором заказа для получения его статуса.
     *
     * @param int|string $id Идентификатор заказа.
     * @return array Ответ от API в виде массива со статусом заказа.
     * @throws Exception Исключение при неудачном запросе к API.
     */
    public static function getStatus(int|string $id): array
    {
        return static::callTester('status', ['order_id' => $id]);
    }

    /**
     * Создает новый заказ в системе Paloma.
     *
     * Вызывает метод API 'order' для создания заказа. Использует класс Point для получения идентификатора точки.
     *
     * @param string $point Название точки, где будет выполнен заказ.
     * @param array $order Данные заказа.
     * @return array Ответ от API в виде массива с информацией о созданном заказе.
     * @throws Exception Исключение при неудачном запросе к API.
     */
    public static function add(string $point, array $order): array
    {
        return static::callTester('order', [
            'point_id' => Point::getByName($point)
        ], $order, true);
    }
}

<?php

namespace CNTL\Api\Paloma;

use Exception;

/**
 * Класс Product
 * Расширяет функциональность базового класса BaseEntity для работы с товарами Paloma.
 */
class Product extends BaseEntity
{
    /**
     * Получает список товаров из Paloma.
     *
     * Вызывает метод callGuide2XML с параметром 's_items' для получения списка товаров.
     *
     * @throws Exception Исключение, возникающее при ошибке запроса к API.
     * @return array Массив товаров, полученный из Paloma.
     */
    public static function getList(): array
    {
        return static::callGuide2XML('s_items');
    }
}

<?php

namespace CNTL\Api\Paloma;

use Exception;

/**
 * Класс Point
 * Расширяет функциональность базового класса BaseEntity для работы с точками Paloma.
 */
class Point extends BaseEntity
{
    /**
     * Получает список точек из Paloma.
     *
     * Вызывает метод callTester с параметром 'points' для получения списка точек.
     *
     * @return array Массив точек, полученный из Paloma.
     * @throws Exception Исключение, возникающее при ошибке запроса к API.
     */
    public static function getList(): array
    {
        return static::callTester('points');
    }

    /**
     * Получает идентификатор точки по её названию.
     *
     * Использует метод getList для получения списка точек, а затем фильтрует его,
     * чтобы найти точку с указанным названием. Возвращает идентификатор найденной точки.
     *
     * @param string $name Название точки для поиска.
     * @return string Идентификатор найденной точки или 0, если точка не найдена.
     * @throws Exception Исключение, возникающее при ошибке запроса к API.
     */
    public static function getByName(string $name): string
    {
        $points = static::getList();
        $point = array_map(function ($point) {
            return $point['point_id'];
        }, array_filter($points, function ($point) use ($name) {
            return $point['name'] === $name;
        }));
        return $point[array_keys($point)[0]] ?? 0;
    }
}
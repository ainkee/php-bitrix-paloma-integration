<?php

namespace CNTL\ProductCatalog;

class ProductMapper
{
    public function mapToCatalog(array $productData): Catalog
    {
        return new Catalog($productData['UID'], $productData['name']);
    }

    public function mapToItem(array $productData): Item
    {
        return new Item($productData['UID'], $productData['name'], $productData['price'], $productData['i_useInMenu'], $productData['key']);
    }
}
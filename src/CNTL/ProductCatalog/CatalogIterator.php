<?php

namespace CNTL\ProductCatalog;

use CNTL\Callback\CatalogCallbackInterface;

class CatalogIterator
{
    private array $rootItems;
    private CatalogCallbackInterface $catalogCallback;
    private CatalogCallbackInterface $itemCallback;

    public function __construct(array $rootItems, CatalogCallbackInterface $catalogCallback, CatalogCallbackInterface $itemCallback)
    {
        $this->rootItems = $rootItems;
        $this->catalogCallback = $catalogCallback;
        $this->itemCallback = $itemCallback;
    }

    public function iterate(): void
    {
        foreach ($this->rootItems as $item) {
            $this->iterateItem($item);
        }
        $this->itemCallback->finish();
    }

    private function iterateItem(CatalogItem $item): void
    {
        if ($item instanceof Catalog) {
            $this->catalogCallback->execute($item);
            foreach ($item->children as $child) {
                $this->iterateItem($child);
            }
        } elseif ($item instanceof Item) {
            $this->itemCallback->execute($item);
        }
    }
}
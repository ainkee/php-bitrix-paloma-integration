<?php

namespace CNTL\ProductCatalog;

class ProductTreeSorter
{
    private array $tree = [];
    private array $catalogs;
    private array $items;
    private array $products;

    public function __construct(array $catalogs, array $items, array $products)
    {
        $this->catalogs = $catalogs;
        $this->items = $items;
        $this->products = $products;
    }

    /**
     * Сортирует элементы дерева.
     *
     * @return array
     */
    public function sort(): array
    {
        foreach ($this->products as $product) {
            $uid = $product['UID'];
            $parentUid = $product['parentid'] ?: ProductTree::$rootCatalogId;

            if (isset($this->catalogs[$uid])) {
                $this->addCatalog($parentUid, $uid);
            } elseif (isset($this->items[$uid])) {
                $this->addItem($parentUid, $this->items[$uid], $uid);
            }
        }

        return $this->tree;
    }

    /**
     * Добавляет каталог в древовидную структуру.
     *
     * @param mixed $parentUid Идентификатор родительского каталога.
     * @param mixed $uid Идентификатор текущего каталога.
     *
     * @return void
     */
    public function addCatalog(mixed $parentUid, mixed $uid): void
    {
        if ($parentUid !== ProductTree::$rootCatalogId && isset($this->catalogs[$parentUid])) {
            $this->catalogs[$parentUid]->add($this->catalogs[$uid]);
        } else {
            $this->tree[$uid] = $this->catalogs[$uid];
        }
    }

    /**
     * Добавляет товар в древовидную структуру.
     *
     * @param mixed $parentUid Идентификатор родительского каталога.
     * @param CatalogItem $item Объект товара.
     * @param mixed $uid Идентификатор товара.
     *
     * @return void
     */
    public function addItem(mixed $parentUid, CatalogItem $item, mixed $uid): void
    {
        if (isset($this->catalogs[$parentUid])) {
            $this->catalogs[$parentUid]->add($item);
        } else {
            $this->tree[$uid] = $item;
        }
    }
}
<?php

namespace CNTL\ProductCatalog;

/**
 * Абстрактный класс CatalogItem
 * Является компонентом паттерна "Композит" для представления элементов каталога.
 */
abstract class CatalogItem
{
    /**
     * Родительский элемент каталога.
     * @var CatalogItem|null
     */
    protected ?CatalogItem $parent = null;


    /**
     * Внешний идентификатор элемента каталога.
     * @var string
     */
    protected string $externalId = '';
    /**
     * Уникальный идентификатор элемента каталога.
     * @var string
     */
    public string $uid;

    /**
     * Название элемента каталога.
     * @var string
     */
    public string $name;

    /**
     * Цена элемента (для товаров).
     * @var string
     */
    public string $price;

    /**
     * Активность элемента.
     * @var string
     */
    public string $active;

    /**
     * Устанавливает внешний ID элемента
     *
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * Возвращает внешний ID элемента
     *
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * Устанавливает родительский элемент для текущего элемента каталога.
     *
     * @param CatalogItem|null $parent Родительский элемент каталога.
     */
    public function setParent(?CatalogItem $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * Возвращает родительский элемент текущего элемента каталога.
     *
     * @return CatalogItem|null Родительский элемент.
     */
    public function getParent(): ?CatalogItem
    {
        return $this->parent ?? null;
    }

    /**
     * Добавляет подкомпонент к текущему элементу.
     *
     * @param CatalogItem $component Подкомпонент для добавления.
     */
    public function add(CatalogItem $component): void
    {

    }

    /**
     * Удаляет подкомпонент из текущего элемента.
     *
     * @param CatalogItem $component Подкомпонент для удаления.
     */
    public function remove(CatalogItem $component): void
    {

    }

    /**
     * Определяет, является ли текущий элемент композитным (т.е. может содержать другие элементы).
     *
     * @return bool Возвращает true, если элемент может содержать подкомпоненты.
     */
    public function isComposite(): bool
    {
        return false;
    }
}

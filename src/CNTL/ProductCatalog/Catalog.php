<?php

namespace CNTL\ProductCatalog;

use SplObjectStorage;

/**
 * Класс Catalog
 * Расширяет функциональность CatalogItem, представляя собой композитный элемент в паттерне "Композит".
 */
class Catalog extends CatalogItem
{
    /**
     * Хранилище для дочерних элементов каталога.
     * @var SplObjectStorage
     */
    public SplObjectStorage $children;

    /**
     * Конструктор класса Catalog.
     * Инициализирует каталог с уникальным идентификатором и названием.
     *
     * @param string $uid Уникальный идентификатор каталога.
     * @param string $name Название каталога.
     */
    public function __construct(string $uid, string $name)
    {
        $this->uid = $uid;
        $this->name = $name;
        $this->children = new SplObjectStorage();
    }

    /**
     * Добавляет подкомпонент к каталогу.
     *
     * @param CatalogItem $component Подкомпонент для добавления.
     */
    public function add(CatalogItem $component): void
    {
        $this->children->attach($component);
        $component->setParent($this);
    }

    /**
     * Удаляет подкомпонент из каталога.
     *
     * @param CatalogItem $component Подкомпонент для удаления.
     */
    public function remove(CatalogItem $component): void
    {
        $this->children->detach($component);
        $component->setParent(null);
    }

    /**
     * Определяет, является ли каталог композитным элементом.
     *
     * @return bool Возвращает true, указывая на то, что каталог может содержать подкомпоненты.
     */
    public function isComposite(): bool
    {
        return true;
    }
}
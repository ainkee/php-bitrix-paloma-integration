<?php

namespace CNTL\ProductCatalog;

/**
 * Класс Item
 * Представляет собой листовой элемент в паттерне "Композит", используемый для представления товаров.
 */
class Item extends CatalogItem
{
    public string $objectId;

    /**
     * Конструктор класса Item.
     * Инициализирует товар с уникальным идентификатором, названием, ценой и статусом активности.
     *
     * @param string $uid Уникальный идентификатор товара.
     * @param string $name Название товара.
     * @param string $price Цена товара.
     * @param string $active Статус активности товара.
     * @param string $objectId ID товара в Paloma365.
     */
    public function __construct(string $uid, string $name, string $price, string $active, string $objectId)
    {
        $this->uid = $uid;
        $this->name = $name;
        $this->price = $price;
        $this->active = $active == "1" ? "Y" : "N";
        $this->objectId = $objectId;
    }
}
<?php

namespace CNTL\ProductCatalog;

/**
 * Класс ProductTree предназначен для создания древовидной структуры каталогов и товаров.
 */
class ProductTree
{
    /**
     * Корневой идентификатор каталога.
     * @var int
     */
    public static int $rootCatalogId = 0;
    private array $tree;
    private array $products;

    /**
     * Конструктор класса ProductTree.
     * Инициализирует объекты каталогов и товаров на основе переданных данных.
     *
     * @param array $products Массив данных о продуктах.
     */
    public function __construct(array $products)
    {
        $this->products = $products["s_items"];
    }

    /**
     * Строит древовидную структуру.
     */
    private function build(): void
    {
        $mapper = new ProductMapper();
        $builder = new ProductTreeBuilder($this->products, $mapper);
        $builder->build();

        $sorter = new ProductTreeSorter($builder->getCatalogs(), $builder->getItems(), $this->products);
        $this->tree = $sorter->sort();
    }

    /**
     * Возвращает древовидную структуру.
     */
    public function get(): array
    {
        if (empty($this->tree)) {
            $this->build();
        }

        return $this->tree;
    }
}
<?php

namespace CNTL\ProductCatalog;

class ProductTreeBuilder
{
    private array $products;
    private array $catalogs = [];
    private array $items = [];
    private ProductMapper $mapper;

    public function __construct(array $products, ProductMapper $mapper)
    {
        $this->products = $products;
        $this->mapper = $mapper;
    }

    /**
     * Строит древовидную структуру.
     */
    public function build(): void
    {
        $this->setCatalogItems();
    }

    public function getCatalogs(): array
    {
        return $this->catalogs;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * Проверяет, является ли продукт группой (каталогом).
     *
     * @param array $product Данные продукта.
     * @return bool Возвращает true, если продукт является группой.
     */
    private function isGroup(array $product): bool
    {
        return $product['isgroup'] === '1';
    }

    /**
     * Инициализирует каталоги и товары.
     *
     * @return void
     */
    public function setCatalogItems(): void
    {
        foreach ($this->products as $key => $product) {
            $uid = $product['UID'];

            if ($this->isGroup($product)) {
                $this->catalogs[$uid] = $this->mapper->mapToCatalog($product);
            } else {
                $product['key'] = $key;
                $this->items[$uid] = $this->mapper->mapToItem($product);
            }
        }
    }
}
<?php

use CNTL\Manager\ProductManager;

require_once __DIR__ . '/init.php';

$manager = new ProductManager();

try {
    $manager->synchronize();
} catch (Exception $e) {
    echo $e->getMessage();
}
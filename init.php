<?php
require_once __DIR__ . '/vendor/autoload.php';

date_default_timezone_set('Asia/Almaty');

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

function writeToLog($text): void
{
    $filename = "log.txt";
    $log = date('Y-m-d H:i:s' . "\n") . print_r($text, true);
    file_put_contents($filename, $log . PHP_EOL, FILE_APPEND);
}